const express = require('express');
const router = express.Router();
const productController = require('../controllers/productController');
const auth = require('../auth');

// Routes
router.post("/addProduct", auth.verify, (request, response) => {
	const userData = auth.decode(request.headers.authorization).isAdmin

	productController.addProduct(request.body, userData).then(resultFromController => response.send(resultFromController))
});

router.get("/activeProducts", (request, response) => {
	productController.activeProducts().then(resultFromController => response.send(resultFromController))
});

router.get("/inactiveProducts", (request, response) => {
	productController.inactiveProducts().then(resultFromController => response.send(resultFromController))
});

router.get("/getProduct", (request, response) => {
	
	productController.isProduct(request.body).then(resultFromController => response.send(resultFromController))
});

router.put("/updateProduct", auth.verify, (request, response) => {
	const userData = auth.decode(request.headers.authorization).isAdmin

	productController.updateProduct(request.body, userData).then(resultFromController => response.send(resultFromController))
});

router.put("/archiveProduct", auth.verify, (request, response) => {
	const userData = auth.decode(request.headers.authorization).isAdmin

	productController.updateStatus(request.body, userData).then(resultFromController => response.send(resultFromController))
});

module.exports = router;