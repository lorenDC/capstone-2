const Products = require('../models/products');
const Users = require('../models/users');
const auth = require('../auth');

// Post functions
// Add Products
module.exports.addProduct = async (request, checkAdmin) => {
	if (checkAdmin == false) {
		return ("You are NOT an admin!")
	} else {
		let newProducts = new Products ({
				name: request.name,
				description: request.description,
				price: request.price,
				quantity: request.quantity
			})
		return newProducts.save()
		.then(result => {return result})
		.catch(error => {return error})		
		}
};

// Retrieve Active Products
module.exports.activeProducts = () => {
	return Products.find({isActive: true})
	.then(result => {return result})
	.catch(error => {return error})
};

module.exports.inactiveProducts = () => {
	return Products.find({isActive: false})
	.then(result => {return result})
	.catch(error => {return error})
};

// Retrieve certain product
module.exports.isProduct = (request) => {
	return Products.findById(request.id)
	.then(result => {return result})
	.catch(error => {return error})	
	};

// Update product description
module.exports.updateProduct = async (request, checkAdmin) => {
	if (checkAdmin){
		let updatedProduct = {
			name: request.name,
			description: request.description,
			price: request.price
		}
		return Products.findByIdAndUpdate(request.id, updatedProduct).then(result => {return result})
			.catch(error => {return error})
	} else {
		return ("You are not an Admin!")
	}
};

// Archive Product
module.exports.updateStatus = async (request, checkAdmin) => {
	if (checkAdmin){
		let updatedStatus = {
			quantity: request.quantity,
			isActive: request.isActive
		}
		return Products.findByIdAndUpdate(request.id, updatedStatus)
		.then(result => {result})
		.catch(error => {error})
		
	} else {
		return ("You are not an Admin!")
	}
};