const jwt = require('jsonwebtoken');

const secret = "ECommerceAPI"

module.exports.createToken = (user) => {
	const data = {
		id: user._id,
		email: user.email,
		isAdmin: user.isAdmin
	}
	return jwt.sign(data, secret, {})
};

module.exports.verify = (request, response, next) => {

	let token = request.headers.authorization
	// console.log(token)

	if(typeof token !== "undefined") {
		token = token.slice(7, token.length)
		return jwt.verify(token, secret, (error, data) => {
			if(error){
				return response.send({auth: "You encountered an error!"})
			} else {
				next()
			}
		})
	} else {
		return response.send({auth: "You encountered an error!"})
	}
};

module.exports.decode = (token) => {
	if(typeof token !== "undefined") {
		token = token.slice(7, token.length)
		return jwt.verify(token, secret, (error, data) => {
			if(error){
				return response.send({auth: "You encountered an error!"})
			} else {
				return jwt.decode(token, {complete: true}.payload)
			}
		})
	} else {
		return response.send({auth: "You encountered an error!"})
	}
};

