const Orders = require('../models/orders')
const Products = require('../models/products')
const Users = require('../models/users')
const auth = require('../auth')

module.exports.createOrders = async (request, user) => {
	if(user.isAdmin){
		return ("You are an admin!")
	} else {
		let newOrders = new Orders ({
			buyer: request.buyer.map(buyer => {
				return {
					fullname: buyer.fullname,
					userId: user.id,
					userEmail: user.email
				}
				}),
			product: request.product.map(product => {
				return {
					name: product.name,
					productId: product.productId,
					quantity: product.quantity,
					price: product.price
				}
				}),

			totalAmount: (request.product[0].quantity) * (request.product[0].price)
		})
		return newOrders.save() 
		.then(result => {return result})
		.catch(error => {return error})
	}
};