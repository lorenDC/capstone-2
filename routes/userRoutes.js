const express = require('express');
const router = express.Router();
const userController = require('../controllers/userController');
const auth = require('../auth')

// Routes
// Registration
router.post("/registration", (request,response) => {
	
	userController.registration(request.body).then(resultFromController => response.send(resultFromController))
});

// Authentication
router.post("/getToken", (request, response) => {
	
	userController.getToken(request.body).then(resultFromController => response.send(resultFromController))
});

router.get("/checkAccess", auth.verify, (request, response) => {
	const userData = auth.decode(request.headers.authorization).isAdmin
	
	userController.checkUser(userData).then(resultFromController => response.send(resultFromController))
});

router.get("/userDetails", auth.verify, (request, response) => {
	const isAdmin = auth.decode(request.headers.authorization)

	userController.userDetails(isAdmin).then(resultFromController => response.send(resultFromController))
})

router.get("/orderDetails", auth.verify, (request, response) => {
	const isAdmin = auth.decode(request.headers.authorization)

	userController.orderDetails(isAdmin).then(resultFromController => response.send(resultFromController))
})

router.put("/updateUser", auth.verify, (request, response) => {
	const isAdmin = auth.decode(request.headers.authorization)

	userController.setUser(request.body, isAdmin).then(resultFromController => response.send(resultFromController))
})

module.exports = router;